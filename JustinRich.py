import konane
import sys
import random

class JustinRichMinimaxPlayer(konane.Konane, konane.Player):

    def __init__(self, size, depthLimit):
        konane.Konane.__init__(self, size)
        self.limit = depthLimit
        self.moveToMake = []

    def initialize(self, side):
        self.side = side
        self.name = "Justin and Rich Minimax"
        self.INFINITY = sys.maxint
        self.alpha = self.beta = 0
		
    def getMove(self, board):
        self.alpha = -self.INFINITY
        self.beta = self.INFINITY
        self.moveToMake = []
        self.minimax(board, self.staticEvaluate)
        return self.moveToMake
	
    def minimax(self, board, evalFun):
        result = self.minimaxEvaluate (board, self.staticEvaluate)
        if len(result) == 2:
            self.moveToMake = result[1]
        return result[0]
        
    def staticEvaluate (self, board, player):
        m = len(konane.Konane.generateMoves(self, board, player))
        n = len(konane.Konane.generateMoves(self,board,self.opponent(player)))
        if n == 0:
            return sys.maxint
        if m == 0:
            return -sys.maxint -1
        return ((m)/n)
	
    def minimum (self, value1, value2, isAB = 0):
        if isAB:
            return value1 < value2
        elif value1 < value2:
            self.beta = value1
            return 1
        else:
           return 0

    def maximum (self, value1, value2, isAB = 0):
        if isAB:
            return value1 > value2
        elif value1 > value2:
            self.alpha = value1
            return 1
        else:
            return 0

    def getCurrentPlayer (self, depth):
        if depth % 2 == 0:
            return self.side
        else:
            return self.opponent(self.side)

    def getMaximumValue(self, player):
        if player == self.side:
            return -self.INFINITY
        else:
            return self.INFINITY

    def getTestFunction(self, player):
        if player == self.side:
            return self.maximum
        else:
            return self.minimum

    def minimaxEvaluate (self, board, evaluationFunction, depth = 0):
        player = self.getCurrentPlayer(depth)
        maxValue = self.getMaximumValue(player)
        testFunction = self.getTestFunction(player)
        if depth == self.limit:
            return [evaluationFunction(board, player)]
        else:
            bestMoveAvailable = [maxValue]
            bestMoveIndecies = []
            moves = self.generateMoves(board, player)
            for index in range(len(moves)):
                move = moves[index]
                nextBoard = self.nextBoard(board, player, move)
                subtreeEvaluation = self.minimaxEvaluate (nextBoard, evaluationFunction, depth + 1)
                if subtreeEvaluation[0] == bestMoveAvailable[0]:
                    bestMoveIndecies.append(index)
                elif testFunction(subtreeEvaluation[0], bestMoveAvailable[0]):
                    bestMoveIndecies = [index]
                    bestMoveAvailable = [subtreeEvaluation[0]]
                    
                if testFunction(self.beta, self.alpha, 1):
                    break
		    		
            if bestMoveIndecies != []:
                count = len(bestMoveIndecies)
                randomIndex = random.randrange(0, count)
                indexOfMove = bestMoveIndecies[randomIndex]
                move = moves[indexOfMove]
                bestMoveAvailable.append(move)
			
            return bestMoveAvailable
