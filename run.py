import konane
import TomZachMinimaxPlayer
import TomZachMinimaxPlayer2
import BillMurray
import Mitch
import JustinRich

numberOfGames = 10
size = 8
depth = 4
display = 0
player = TomZachMinimaxPlayer.TomZachMinimaxPlayer(size, depth)
opponent = JustinRich.JustinRichMinimaxPlayer(size, depth)

game = konane.Konane(size)
game.playNGames(numberOfGames, player, opponent, display)
