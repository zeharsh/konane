import konane
import random
import copy
import sys
from distutils.tests.setuptools_build_ext import if_dl

class TomZachMinimaxPlayer(konane.Konane, konane.Player): 
 
    def __init__(self, size, depthLimit):
        konane.Konane.__init__(self, size)
        self.limit = depthLimit
		
    def initialize(self, side):
        self.side= side;
        self.name = "Tom Zach Minimax"
 
    def getMove(self, board):
        bestMove = []
        bestScore = -sys.maxint - 1
        for move in self.generateMoves(board, self.side):
            boardPot = self.nextBoard(board, self.side, move)
            if self.openingMove(boardPot):
                return move
            else:
                boardScore = self.alphaBeta(boardPot, 0, -sys.maxint - 1, sys.maxint, True)
                if boardScore >= bestScore:
                    bestScore = boardScore
                    bestMove = move
        return bestMove
    
    def getBoards(self, board, player):
        boards = []
        for move in self.generateMoves(board, player):
            boards.append(self.nextBoard(board, player, move))
        return boards
    
    def alphaBeta(self, board, depth, alpha, beta, isMaximizing):
        if depth > self.limit or len(self.generateMoves(board, self.side))== 0:
            if isMaximizing:
                return self.staticEval(board, self.side)
            else:
                
                return self.staticEval(board, self.opponent(self.side))
        if isMaximizing:
            boardList = self.getBoards(board,self.side)
            for board in boardList:
                alpha = max(alpha,self.alphaBeta(board, depth + 1, alpha, beta, False))
                if beta <= alpha:
                    break
            return alpha
        else:
            boardList = self.getBoards(board, self.opponent(self.side))
            for board in boardList:
                beta = min(beta,self.alphaBeta(board, depth + 1, alpha, beta, True))
                if beta <= alpha:
                    break
            return beta
	
    def miniMax(self, board, alpha, beta):
        return self.maxMove(board, alpha, beta, 0)
		
    def bestMove(self, board, moves, player):
        bestMove = moves[0]
        for x in moves:
            newScore = self.staticEval(self.nextBoard(board, player, x), player)
            oldScore = self.staticEval(self.nextBoard(board, player, bestMove), player)
            if newScore > oldScore:
                bestMove = x
        return bestMove
		
    def maxMove(self, board, alpha, beta,depth):
        if len(self.generateMoves(board, self.side)) == 0 or depth > self.limit:
            return []
        moves = self.generateMoves(board, self.side)
        bestMove = []
        bestBoard = []
        for x in moves:
            opponentsPotentialBoard = self.nextBoard(board, self.side, x)
            opponentsMove = self.minMove(opponentsPotentialBoard, alpha, beta, depth + 1)
            if opponentsMove == []:
                return self.bestMove(board, moves, self.side)
            myPotentialBoard = self.nextBoard(opponentsPotentialBoard, self.opponent(self.side), opponentsMove)
            if self.staticEval(myPotentialBoard, self.side) > self.staticEval(bestBoard, self.side):
                bestMove = x
                bestBoard = myPotentialBoard
                alpha = max(alpha, self.staticEval(bestBoard, self.side))
                if beta >= alpha:
                    return bestMove
        return bestMove
		
    def minMove(self, board, alpha, beta, depth):
        if len(self.generateMoves(board, self.opponent(self.side))) == 0 or depth > self.limit:
            return []
        moves = self.generateMoves(board, self.opponent(self.side))
        bestMove = []
        bestBoard = []
        for x in moves:
            opponentsPotentialBoard = self.nextBoard(board, self.opponent(self.side), x)
            opponentsMove = self.maxMove(opponentsPotentialBoard, alpha, beta, depth + 1)
            if opponentsMove == []:
                return self.bestMove(board, moves, self.opponent(self.side))
            myPotentialBoard = self.nextBoard(opponentsPotentialBoard, self.side, opponentsMove)
            if self.staticEval(myPotentialBoard, self.opponent(self.side)) < self.staticEval(bestBoard, self.opponent(self.side)):
                bestMove = x
                bestBoard = myPotentialBoard
                beta = min(self.staticEval(bestBoard, self.opponent(self.side)))
                if beta >= alpha:
                    return bestMove
        return bestMove        
	
    def staticEval(self, board, player):
        m = len(konane.Konane.generateMoves(self, board, player))
        n = len(konane.Konane.generateMoves(self, board, self.opponent(player)))
        if n == 0:
            return sys.maxint
        if m == 0:
            return -sys.maxint - 1
        return ((m + 0.0) / n)
