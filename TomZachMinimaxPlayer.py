import konane
import random
import copy
import sys

"""
Tom Hlavaty
Zach Harsh
"""

class TomZachMinimaxPlayer(konane.Konane, konane.Player):

    def __init__(self, size, depthLimit):
        konane.Konane.__init__(self, size)
        self.limit = depthLimit
        self.moveToMake = []
        
    def initialize(self, side):
        self.side = side
        self.name = "Tom and Zach Minimax"
	
    def getMove(self, board):
        self.alpha = -sys.maxint
        self.beta = sys.maxint
        self.moveToMake = []
        self.minimax(board)
        return self.moveToMake
        
    def staticEval(self, board, player):
        p = len(konane.Konane.generateMoves(self, board, player))
        o = len(konane.Konane.generateMoves(self, board, self.opponent(player)))
        if o == 0:
            return sys.maxint
        if p == 0:
            return -sys.maxint - 1
        return ((p + 0.0) / o)
	
    def minimum(self, value1, value2, isBeta = 0):
        if isBeta:
            return value1 < value2
        elif value1 < value2:
            self.beta = value1
            return 1
        else:
            return 0
        
    def maximum(self, value1, value2, isAlpha = 0):
        if isAlpha:
            return value1 > value2
        elif value1 > value2:
            self.alpha = value1
            return 1
        else:
            return 0
        
    def minimax(self, board):
        result = self.minimaxEvaluate(board, self.staticEval)
        if len(result) == 2:
            self.moveToMake = result[1]
        return result[0]
        
    def minimaxEvaluate(self, board, evaluationFunction, depth = 0):
        if depth % 2 == 0:
            player = self.side
        else:
            player = self.opponent(self.side)
        if player == self.side:
            maxValue = -sys.maxint
        else:
            maxValue = sys.maxint
        if player == self.side:
            testFunction = self.maximum
        else:
            testFunction = self.minimum
        if depth == self.limit:
            return [evaluationFunction(board, player)]
        else:
            bestMoveAvailable = [maxValue]
            bestMoveIndecies = []
            moves = self.generateMoves(board, player)
            for index in range(len(moves)):
                move = moves[index]
                nextBoard = self.nextBoard(board, player, move)
                subtreeEvaluation = self.minimaxEvaluate(nextBoard, evaluationFunction, depth + 1)
                if subtreeEvaluation[0] == bestMoveAvailable[0]:
                    bestMoveIndecies.append(index)
                elif testFunction(subtreeEvaluation[0], bestMoveAvailable[0]):
                    bestMoveIndecies = [index]
                    bestMoveAvailable = [subtreeEvaluation[0]]

                if testFunction(self.beta, self.alpha, 1):
                    break			
            if bestMoveIndecies != []:
                count = len(bestMoveIndecies)
                randomIndex = random.randrange(0, count)
                indexOfMove = bestMoveIndecies[randomIndex]
                move = moves[indexOfMove]
                bestMoveAvailable.append(move)		
            return bestMoveAvailable
