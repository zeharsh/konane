### Tom Hlavaty
### Zach Harsh
### CSCI 450
### Dr. Schwartz

### File: konane.py
### Classes defined: KonaneError, Konane, Player, SimplePlayer,
### RandomPlayer, HumanPlayer, MinimaxPlayer

import random
import copy
import sys
from distutils.tests.setuptools_build_ext import if_dl

class KonaneError(AttributeError):
    """
    This class is used to indicate a problem in the konane game.
    """

class Konane:
    """
    This class implements Konane, the Hawaiian version of checkers.
    The board is represented as a two-dimensional list.  Each 
    location on the board contains one of the following symbols:
       'B' for a black piece
       'W' for a white piece
       '.' for an empty location
    The black player always goes first.  The opening moves by both
    players are special cases and involve removing one piece from
    specific designated locations.  Subsequently, each move is a
    jump over one of the opponent's pieces into an empty location.
    The jump may continue in the same direction, if appropriate.
    The jumped pieces are removed, and then it is the opponent's
    turn.  Play continues until one player has no possible moves,
    making the other player the winner. 
    """
    def __init__(self, n):
        self.size = n
        self.reset()
        
    def reset(self):
        """
        Resets the starting board state.
        """
        self.board = []
        value = 'B'
        for i in range(self.size):
            row = []
            for j in range(self.size):
                row.append(value)
                value = self.opponent(value)
            self.board.append(row)
            if self.size%2 == 0:
                value = self.opponent(value)

    def __str__(self):
        return self.boardToStr(self.board)

    def boardToStr(self, board):
        """
        Returns a string representation of the konane board.
        """
        result = "  "
        for i in range(self.size):
            result += str(i) + " "
        result += "\n"
        for i in range(self.size):
            result += str(i) + " "
            for j in range(self.size):
                result += str(board[i][j]) + " "
            result += "\n"
        return result

    def valid(self, row, col):
        """
        Returns true if the given row and col represent a valid location on
        the konane board.
        """
        return row >= 0 and col >= 0 and row < self.size and col < self.size

    def contains(self, board, row, col, symbol):
        """
        Returns true if the given row and col represent a valid location on
        the konane board and that lcoation contains the given symbol.
        """
        return self.valid(row,col) and board[row][col]==symbol

    def countSymbol(self, board, symbol):
        """
        Returns the number of instances of the symbol on the board.
        """
        count = 0
        for r in range(self.size):
            for c in range(self.size):
                if board[r][c] == symbol:
                    count += 1
        return count

    def opponent(self, player):
        """
        Given a player symbol, returns the opponent's symbol, 'B' for black,
        or 'W' for white.
        """
        if player == 'B':
            return 'W'
        else:
            return 'B'

    def distance(self, r1, c1, r2, c2):
        """
        Returns the distance between two points in a vertical or
        horizontal line on the konane board.
        """
        return abs(r1-r2 + c1-c2)

    def makeMove(self, player, move):
        """
        Updates the current board with the next board created by the given
        move.
        """
        self.board = self.nextBoard(self.board, player, move)

    def nextBoard(self, board, player, move):
        """
        Given a move for a particular player from (r1,c1) to (r2,c2) this
        executes the move on a copy of the current konane board.  It will
        raise a KonaneError if the move is invalid. It returns the copy of
        the board, and does not change the given board.
        """
        if move == [] or move == None:
            return board;
        r1 = move[0]
        c1 = move[1]
        r2 = move[2]
        c2 = move[3]
        next = copy.deepcopy(board)
        if not (self.valid(r1, c1) and self.valid(r2, c2)):
            raise KonaneError
        if next[r1][c1] != player:
            raise KonaneError
        dist = self.distance(r1, c1, r2, c2)
        if dist == 0:
            if self.openingMove(board):
                next[r1][c1] = "."
                return next
            raise KonaneError
        if next[r2][c2] != ".":
            raise KonaneError
        jumps = dist//2
        dr = (r2 - r1)//dist
        dc = (c2 - c1)//dist
        for i in range(jumps):
            if next[r1+dr][c1+dc] != self.opponent(player):
                raise KonaneError
            next[r1][c1] = "."
            next[r1+dr][c1+dc] = "."
            r1 += 2*dr
            c1 += 2*dc
            next[r1][c1] = player
        return next

    def openingMove(self, board):
        """
        Based on the number of blanks present on the konane board, determines
        whether the current move is the first or second of the game.
        """
        return self.countSymbol(board, ".") <= 1

    def generateFirstMoves(self, board):
        """
        Returns the special cases for the first move of the game.
        """
        moves = []
        moves.append([0]*4)
        moves.append([self.size-1]*4)
        moves.append([self.size/2]*4)
        moves.append([(self.size/2)-1]*4)
        return moves

    def generateSecondMoves(self, board):
        """
        Returns the special cases for the second move of the game, based
        on where the first move occurred.
        """
        moves = []
        if board[0][0] == ".":
            moves.append([0,1]*2)
            moves.append([1,0]*2)
            return moves
        elif board[self.size-1][self.size-1] == ".":
            moves.append([self.size-1,self.size-2]*2)
            moves.append([self.size-2,self.size-1]*2)
            return moves
        elif board[self.size/2-1][self.size/2-1] == ".":
            pos = self.size/2 -1
        else:
            pos = self.size/2
        moves.append([pos,pos-1]*2)
        moves.append([pos+1,pos]*2)
        moves.append([pos,pos+1]*2)
        moves.append([pos-1,pos]*2)
        return moves

    def check(self, board, r, c, rd, cd, factor, opponent):
        """
        Checks whether a jump is possible starting at (r,c) and going in the
        direction determined by the row delta, rd, and the column delta, cd.
        The factor is used to recursively check for multiple jumps in the same
        direction.  Returns all possible jumps in the given direction.
        """
        if self.contains(board,r+factor*rd,c+factor*cd,opponent) and \
           self.contains(board,r+(factor+1)*rd,c+(factor+1)*cd,'.'):
            return [[r,c,r+(factor+1)*rd,c+(factor+1)*cd]] + \
                   self.check(board,r,c,rd,cd,factor+2,opponent)
        else:
            return []

    def generateMoves(self, board, player):
        """
        Generates and returns all legal moves for the given player using the
        current board configuration.
        """
        if self.openingMove(board):
            if player=='B':
                return self.generateFirstMoves(board)
            else:
                return self.generateSecondMoves(board)
        else:
            moves = []
            rd = [-1,0,1,0]
            cd = [0,1,0,-1]
            for r in range(self.size):
                for c in range(self.size):
                    if board[r][c] == player:
                        for i in range(len(rd)):
                            moves += self.check(board,r,c,rd[i],cd[i],1,
                                                self.opponent(player))
            return moves

    def playOneGame(self, p1, p2, show):
        """
        Given two instances of players, will play out a game
        between them.  Returns 'B' if black wins, or 'W' if
        white wins. When show is true, it will display each move
        in the game.
        """
        self.reset()
        p1.initialize('B')
        p2.initialize('W')
        while 1:
            if show:
                print(self)
                print("player B's turn")
            move = p1.getMove(self.board)
            if move == []:
                result = 'W'
                break
            self.makeMove('B', move)
            if show:
                print(move)
                print()
                print(self)
                print("player W's turn")
            move = p2.getMove(self.board)
            if move == []:
                result = 'B'
                break
            self.makeMove('W', move)
            if show:
                print(move)
                print()
        if show:
            print("Game over")
        return result

    def playNGames(self, n, p1, p2, show):
        """
        Will play out n games between player p1 and player p2.
        The players alternate going first.  Prints the total
        number of games won by each player.
        """
        p1.reset()
        p2.reset()
        first = p1
        second = p2
        for i in range(n):
            print("Game", i)
            winner = self.playOneGame(first, second, show)
            if winner == 'B':
                first.won()
                print(first.name, "wins")
            else:
                second.won()
                print(second.name, "wins")
            temp = first
            first = second
            second = temp
        print(first.name, first.wins, second.name, second.wins)
            

class Player:
    """
    A base class for Konane players.  All players must implement
    the the initialize and getMove methods.
    """
    name = "Player"
    wins = 0
    def won(self):
        self.wins += 1
    def reset(self):
        self.wins = 0
    def initialize(self, side):
        """
        Records the player's side, either 'B' for black or
        'W' for white.  Should also set the name of the player.
        """
        abstract()
    def getMove(self, board):
        """
        Given the current board, should return a valid move.
        """
        abstract()

class MinimaxPlayer(Konane, Player): 
 
    def __init__(self, size, depthLimit):
        Konane.__init__(self, size)
        self.limit = depthLimit
		
    def initialize(self, side):
        self.side= side;
        self.name = "Tom Zach Minimax"
 
    def getMove(self, board):
        bestMove = []
        bestScore = -sys.maxint - 1
        for move in self.generateMoves(board, self.side):
            boardPot = self.nextBoard(board, self.side, move)
            if self.openingMove(boardPot):
                return move
            else:
                boardScore = self.alphaBeta(boardPot, 0, -sys.maxint - 1, sys.maxint, True)
                if boardScore >= bestScore:
                    bestScore = boardScore
                    bestMove = move
        return bestMove
    
    def getBoards(self, board, player):
        boards = []
        for move in self.generateMoves(board, player):
            boards.append(self.nextBoard(board, player, move))
        return boards
    
    def alphaBeta(self, board, depth, alpha, beta, isMaximizing):
        if depth > self.limit or len(self.generateMoves(board, self.side))== 0:
            if isMaximizing:
                return self.staticEval(board, self.side)
            else:
                
                return self.staticEval(board, self.opponent(self.side))
        if isMaximizing:
            boardList = self.getBoards(board,self.side)
            for board in boardList:
                alpha = max(alpha,self.alphaBeta(board, depth + 1, alpha, beta, False))
                if beta <= alpha:
                    break
            return alpha
        else:
            boardList = self.getBoards(board, self.opponent(self.side))
            for board in boardList:
                beta = min(beta,self.alphaBeta(board, depth + 1, alpha, beta, True))
                if beta <= alpha:
                    break
            return beta
	
    def miniMax(self, board, alpha, beta):
        return self.maxMove(board, alpha, beta, 0);
		
    def bestMove(self, board, moves, player):
        bestMove = moves[0]
        for x in moves:
            newScore = self.staticEval(self.nextBoard(board, player, x), player)
            oldScore = self.staticEval(self.nextBoard(board, player, bestMove), player)
            if newScore> oldScore:
                bestMove = x
        return bestMove
		
    def maxMove(self,board,alpha,beta,depth):
        if len(self.generateMoves(board, self.side)) == 0 or depth > self.limit:
            return []
        moves = self.generateMoves(board, self.side)
        bestMove = []
        bestBoard = []
        for x in moves:
            opponentsPotentialBoard = self.nextBoard(board, self.side, x)
            opponentsMove = self.minMove(opponentsPotentialBoard, alpha, beta, depth + 1)
            if opponentsMove == []:
                return self.bestMove(board, moves, self.side)
            myPotentialBoard = self.nextBoard(opponentsPotentialBoard, self.opponent(self.side), opponentsMove)
            if self.staticEval(myPotentialBoard, self.side) > self.staticEval(bestBoard, self.side):
                bestMove = x
                bestBoard = myPotentialBoard
                alpha =max(alpha, self.staticEval(bestBoard, self.side))
                if beta >= alpha:
                    return bestMove
        return bestMove
		
    def minMove(self, board, alpha, beta, depth):
        if len(self.generateMoves(board, self.opponent(self.side))) == 0 or depth > self.limit:
            return []
        moves = self.generateMoves(board, self.opponent(self.side))
        bestMove = []
        bestBoard = []
        for x in moves:
            opponentsPotentialBoard = self.nextBoard(board, self.opponent(self.side), x)
            opponentsMove = self.maxMove(opponentsPotentialBoard, alpha, beta, depth + 1)
            if opponentsMove == []:
                return self.bestMove(board, moves, self.opponent(self.side))
            myPotentialBoard = self.nextBoard(opponentsPotentialBoard, self.side, opponentsMove)
            if self.staticEval(myPotentialBoard, self.opponent(self.side)) < self.staticEval(bestBoard, self.opponent(self.side)):
                bestMove = x
                bestBoard = myPotentialBoard
                beta = min(self.staticEval(bestBoard, self.opponent(self.side)))
                if beta >= alpha:
                    return bestMove
        return bestMove        
      
    def staticEval(self, board, player):
        m = len(Konane.generateMoves(self, board, player))
        n = len(Konane.generateMoves(self, board, self.opponent(player)))
        if n == 0:
            return sys.maxint
        if m == 0:
            return -sys.maxint - 1
        return ((m + 0.0) / n)
            
class SimplePlayer(Konane, Player):
    """
    Always chooses the first move from the set of possible moves.
    """
    def initialize(self, side):
        self.side = side
        self.name = "Simple"
    def getMove(self, board):
        moves = self.generateMoves(board, self.side)
        n = len(moves)
        if n == 0:
            return []
        else:
            return moves[0]

class RandomPlayer(Konane, Player):
    """
    Chooses a random move from the set of possible moves.
    """
    def initialize(self, side):
        self.side = side
        self.name = "Random"
    def getMove(self, board):
        moves = self.generateMoves(board, self.side)
        n = len(moves)
        if n == 0:
            return []
        else:
            return moves[random.randrange(0, n)]

class HumanPlayer(Player):
    """
    Prompts a human player for a move.
    """
    def initialize(self, side):
        self.side = side
        self.name = "Human"
    def getMove(self, board):
        global input
        try: input = raw_input
        except NameError: pass
        r1, c1, r2, c2 = eval(input("Enter r1, c1, r2, c2 (or -1's to concede): "))
        if r1 == -1:
            return []
        return [r1, c1, r2, c2]


game = Konane(8)
game.playNGames(2, MinimaxPlayer(8,4), SimplePlayer(3), 1)
